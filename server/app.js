/*  ----------------------------------------------------------------
    ---------                    IMPORTS                   ---------
    ----------------------------------------------------------------  */

// Getting the app config
const appConfiguration = require('./config/model.js');

// Getting the server
const server = require('./services/server');
const data = require("./data/data");


/*  ----------------------------------------------------------------
    ---------                  FUNCTIONS                   ---------
    ----------------------------------------------------------------  */

/**
 * Uses and runs the database cleanup
 * @return none
 */
function exitHandler() {
    console.log("Closing server")
    process.exit()
}


/*  ----------------------------------------------------------------
    ---------                   RUNTIME                    ---------
    ----------------------------------------------------------------  */

// Launch sequence for the server
data.dataInitialization()
    .then ( () => {
      // If the data is ready...
      // Launches the HTTP server on the specified port
      server.listen(appConfiguration.port, () => {
        console.log('Server is listening on port ' + appConfiguration.port);
      });
    })
    .catch( error => {
      // If the database is not ready
      console.log("Error when launching server.")
      exitHandler();
    });



// Handlers when exiting application
process.on('exit', exitHandler);                //catches normal shutdown
process.on('SIGINT', exitHandler);              //catches ctrl+c event
process.on('SIGUSR1', exitHandler);             //catches "kill pid"
process.on('SIGUSR2', exitHandler);             //catches "kill pid"
process.on('uncaughtException', exitHandler);   //catches uncaught exceptions
