/*  ----------------------------------------------------------------
    ---------                    IMPORTS                   ---------
    ----------------------------------------------------------------  */

// Getting the app config
const appConfiguration = require('../config/model.js');

// For file system capabilities
const fs = require('fs');

// HTTP server, with or without TLS support (as configured)
const http = require('http');
const https = require('https');

// Express, Web server framework
const express = require('express');

// Secure mechanisms for Express
const helmet = require('helmet');

// HTTP request body parser
const bodyParser = require('body-parser');

// CORS policy
const cors = require('cors');

/*  ----------------------------------------------------------------
    ---------                     DATA                     ---------
    ----------------------------------------------------------------  */

// TLS options for the HTTPS server
const httpsOptions = {
    key: fs.readFileSync(appConfiguration.tls.key, 'utf-8'),
    cert: fs.readFileSync(appConfiguration.tls.cert, 'utf-8')
};

// CORS option for the middleware
const corsOptions = {
    origin: 'https://nba.projects.guilo.ch',
    optionsSuccessStatus: 200
};


/*  ----------------------------------------------------------------
    ---------             SERVER AND PROTOCOLS             ---------
    ----------------------------------------------------------------  */

// Express instance
const app = express();

// Linking the HTTP server to Express (TLS usage set in the configuration)
let server;
if (appConfiguration.tls.enabled) {
    server = https.createServer(httpsOptions, app);
} else {
    server = http.createServer(app);
}


/*  ----------------------------------------------------------------
    ---------                   MIDDLEWARES                ---------
    ----------------------------------------------------------------  */

// Enabling requests body parsing
app.use(bodyParser.json());

// Applying security mechanisms
app.use(helmet());

// Applying the defined CORS policy
app.use(cors(corsOptions));


/*  ----------------------------------------------------------------
    ---------                     ROUTES                   ---------
    ----------------------------------------------------------------  */

// Getting Express routes
const routes = require('./routes');

// Applying routes to Express
app.use('/', routes);


/*  ----------------------------------------------------------------
    ---------                     MODULE                   ---------
    ----------------------------------------------------------------  */

// Exporting the module capabilities
module.exports = server;