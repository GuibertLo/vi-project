const fs = require('fs')

let leagues = {}
let players = {}
let teams = {}

const objects = [leagues, players, teams]

const seasons = ['2017-18', '2018-19', '2019-20']

exports.dataInitialization = function() {
    return new Promise((resolve, reject) => {

        seasons.forEach(season => {

            initObjects(season)
                .then( () => {

                    // leagues
                    fs.readFile("data/league_reduced/" + season + "_reduced.json", 'utf8' , (err, file) => {
                        if (err) {
                            reject(err)
                        }
                        leagues[season] = JSON.parse(file)
                    })

                    // Teams
                    readDir("data/teams_reduced/")
                        .then ( teamFiles => {
                            const regex = new RegExp(season, 'g')
                            return teamFiles.filter( ( title ) => title.match(regex))

                        })
                        .then (teamFilesFiltered => {
                            teamFilesFiltered.forEach( leagueFileFiltered => {
                                fs.readFile("data/teams_reduced/" + leagueFileFiltered, 'utf8' , (err, file) => {
                                    if (err) {
                                        reject(err)
                                    }

                                    teams[season].push(JSON.parse(file))
                                })
                            })
                        })
                        .catch( err => {
                            reject(err)
                        })

                    // Players
                    readDir("data/players_reduced/")
                        .then ( playerFiles => {
                            const regex = new RegExp(season, 'g')
                            return playerFiles.filter( ( title ) => title.match(regex))

                        })
                        .then (playerFilesFiltered => {
                            playerFilesFiltered.forEach( playerFileFiltered => {
                                fs.readFile("data/players_reduced/" + playerFileFiltered, 'utf8' , (err, file) => {
                                    if (err) {
                                        reject(err)
                                    }
                                    players[season].push(JSON.parse(file))
                                })
                            })
                        })
                        .catch( err => {
                            reject(err)
                        })
                })

            console.log("Season" + season + " fully loaded.")

        })
        resolve(true)
    })
}

exports.getTeams = function(season) {
    return new Promise((resolve) => {
        if (seasons.includes(season)) {
            resolve(teams[season])
        }
        resolve(teams)
    })
}

exports.getPlayers = function(season) {
    return new Promise((resolve) => {
        if (seasons.includes(season)) {
            resolve(players[season])
        }
        resolve(players)
    })
}

exports.getLeagues = function(season) {
    return new Promise((resolve) => {
        if (seasons.includes(season)) {
            resolve(leagues[season])
        }
        resolve(leagues)
    })
}

function readDir(dir) {
    return new Promise((resolve, reject) => {
        fs.readdir(dir, (err, files) => {
            if (err) {
                console.log(dir)
                reject(err)
            }
            resolve(files)
        })
    })
}

function initObjects(season) {
    return new Promise((resolve) => {
        objects.forEach( object => {
            object[season] = []
        })

        resolve(true)
    })
}