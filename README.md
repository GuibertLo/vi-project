# NBA Stats

NBA Stats est une web app qui permet de visualiser les positions des joueurs de basket (NBA) lorsqu'un shoot est pris. Il est possible de changer la granularité des données, et de visualiser les positions de tous les shoots lors d'une saison, pour une équipe donnée, ou pour un joueur en particulier. 


## Execution

### Backend

```bash
cd server
npm install
node app.js
```

### Frontend
Project setup
```bash
cd app
npm install
```

Compiles and hot-reloads for development
```bash
npm run serve
```

Compiles and minifies for production
```bash
npm run build
```

Lints and fixes files
```bash
npm run lint
```

## Delivrables

[Rapport PDF](/report/build/main.pdf)

[Slides](/presentation.pptx)