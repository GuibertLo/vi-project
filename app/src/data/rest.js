import axios from 'axios';
//import store from './../store';


// Used for transport security
const https = require('https');

// Object used to send requests
// Initialized into the initializeHTTP method
let HTTP;

// Object used to get the right error message
const ERROR_TEXTS = {
    304: { post: "Requête non-modifiée." },
    400: { post: "Requête invalide, avez-vous modifié ses paramètres ?" },
    401: { post: "Problème d'authentification" },
    403: { post: "Action non-autorisée" },
    404: { post: "Ressource non-trouvée." },
    500: { post: "Erreur interne du serveur. Veuillez réessayer plus tard." },
    503: { post: "Serveur inatteignable. Veuillez réessayer plus tard." },
}

const BASE_URL = "http://localhost:3000/"


/* --------------------- METHODS --------------------- */


/**
 * Used to initialize or change the hostname used for requests
 * Gets the stored value into Vuex's store
 * @param None
 * @return None
 */
export function initializeData() {
    //return store.getters.getServerAddressFailed to compile.
    //
    // ./src/data/localData.js
    // Module Error (from ./node_modules/eslint-loader/index.js):
    //
    // /home/loic/projects/nbastats/src/data/localData.js
    //   4:7  error  'fs' is assigned a value but never used  no-unused-vars
    //
    // ✖ 1 problem (1 error, 0 warnings)
    HTTP = axios.create({
        baseURL: BASE_URL,
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
        headers: {'Access-Control-Allow-Origin': '*'},
    });
}


/**
 * Generic way to process server errors
 * @param error,  the error raised by the promise
 * @param type,   string of which action was made to the API (add, set, get, login, logout)
 */
export function processError(error, type) {
    // Variable to store the text
    let messageText;

    // If this is not a connectivity issue
    if (error.response) {
        //store.dispatch('setMessage', ERROR_TEXTS[error.response.status][type]);
        messageText = ERROR_TEXTS[error.response.status][type]

        if (messageText === null) {
            messageText = "Une erreur inconnu est survenue. Veuillez réessayer plus tard."
        }

    } else {
        // Here: connectivity error

        if (error.request) {
            // The request was made but no response was received
            messageText = 'Le serveur en répond pas. Veuillez patienter quelques instants et essayer de rafraîchir la page. Si le problème persiste, veuillez nous contacter.'

        } else {
            // The server cannot be reached.
            messageText = 'Le serveur est inaccessible. Veuillez patienter quelques instants et essayer de rafraîchir la page. Si le problème persiste, veuillez nous contacter.'
        }
    }

    return null;
}


/**
 * Sends a request to the server, for all needed data
 * @return JSON, content with the returned data from the server
 */
export function HTTPGetPlayers(season) {
    if (HTTP === undefined) initializeData()

    let getParameters = ''
    if (season) getParameters = "?season=" + season

    return new Promise((resolve, reject) => {
        HTTP.get("players" + getParameters)
            .then( r => r.data )
            .then( response => {
                resolve(response)
            })
            .catch (function (error) {
                processError(error, "get");
                reject('Error')
            })
    })
}

/**
 * Sends a request to the server, for all needed data
 * @return JSON, content with the returned data from the server
 */
export function HTTPGetLeagues(season) {
    if (HTTP === undefined) initializeData()

    let getParameters = ''
    if (season) getParameters = "?season=" + season

    return new Promise((resolve, reject) => {
        HTTP.get("leagues" + getParameters)
            .then( r => r.data )
            .then( response => {
                resolve(response)
            })
            .catch (function (error) {
                processError(error, "get");
                reject('Error')
            })
    })
}

/**
 * Sends a request to the server, for all needed data
 * @return JSON, content with the returned data from the server
 */
export function HTTPGetTeams(season) {
    if (HTTP === undefined) initializeData()

    let getParameters = ''
    if (season) getParameters = "?season=" + season

    return new Promise((resolve, reject) => {
        HTTP.get("teams" + getParameters)
            .then( r => r.data )
            .then( response => {
                resolve(response)
            })
            .catch (function (error) {
                processError(error, "get");
                reject('Error')
            })
    })
}