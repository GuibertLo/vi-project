import store from './../store';
import axios from 'axios';


export function nbaDataLocalInitialization() {
    return new Promise((resolve, reject) => {

        const seasons = ['201718', '201819', '201920']

        seasons.forEach(season => {

            axios
                .get('/data/league_reduced/' + season + '_reduced.json')
                .then(response => {
                    return response.data
                })
                .then(data => {
                    return data.rowSet
                })
                .then(payload => {
                    store.dispatch('saveLeagues', { payload, season })
                        .then(() => {
                            console.log("League " + season + " loaded in store.")

                            //resolve(true)
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
                .catch(error => {
                    reject(error)
                })

            axios
                .get('/data/players_reduced/' + season + '_list.txt')
                .then(response => {
                    return response.data
                })

        })
    })
}