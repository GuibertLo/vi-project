// Loading the pages used in routes
import PageAbout from "@/pages/PageAbout.vue"
import PageShoots from "@/pages/PageShoots";

// Sets all the routes for this application

/**
 * An entry equals to a route (a path) into the application.
 * path:        the path for accessing the route
 * props:       gives information to display the route entry into the sidebar
 *                - display: if entry has to be displayed
 *                - split:    if the entry include a divider after it
 *                - icon:     a MDI icon, displayed to the left of the route name
 * beforeEnter: rule to access the route, when going to this route
 * redirect:    if route accessed, user redirected to another route
 */
const routes = [
    {
        path: "/",
        redirect: {
            name: "Home"
        },
        props: { display: false },
    },
    {
        path: "/home",
        component: PageShoots,
        name: "Home",
        props: { display: true, icon: "home" },
    },
    {
        path: "/about",
        component: PageAbout,
        name: "About",
        props: { display: true, icon: "help-circle" },
    },
    {
        path: "*",
        component: PageShoots,
        props: { display: false, icon: "cancel" }
    }

];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
 function view(name) {
    var res= require('../components/Dashboard/Views/' + name + '.vue');
    return res;
  };**/

export default routes;

