import Vue from 'vue';
import VueRouter from 'vue-router';

// Routes of the application
import routes from './routes.js';
Vue.use(VueRouter);

// configure router
const router = new VueRouter({
    // Add a class for active route
    linkActiveClass: 'active',
    // All the routes defined
    routes,
});

export default router;
