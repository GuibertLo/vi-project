import Vue from 'vue'
import App from './App.vue'
import vuetify from '@/plugins/vuetify'

// import Vue Router
import router from '@/router/index.js'

// import Vuex
import store from '@/store/index.js'

// import mobile detection
import VueMobileDetection from "vue-mobile-detection";
Vue.use(VueMobileDetection);

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  vuetify,
  store,
  router
}).$mount('#app')
