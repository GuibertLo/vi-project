import { Line } from 'vue-chartjs'

export default {
    extends: Line,
    props: {
        chartdata: {
            type: Object,
            default: null
        },
        options: {
            type: Object,
            maintainAspectRatio: false

        }
    },
    mounted() {
        this.renderChart(this.chartdata, this.options)
    },
    watch: {
        // eslint-disable-next-line no-unused-vars
        "chartdata"(to, from) {
            this.renderChart(this.chartdata, this.options)
        }
    }
}