import { Bar } from 'vue-chartjs'

export default {
    extends: Bar,
    props: {
        chartdata: {
            type: Object,
            default: null
        },
        options: {
            type: Object,
            default: {
                responsive: true,
                maintainAspectRatio: false,
            }
        }
    },
    mounted () {
        this.renderChart(this.chartdata, this.options)
    },
    watch: {
        // eslint-disable-next-line no-unused-vars
        "chartdata"(to, from) {
            this.renderChart(this.chartdata, this.options)
        }
    }
}