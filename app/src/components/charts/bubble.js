import { Bubble } from 'vue-chartjs'

export default {
    extends: Bubble,
    props: {
        chartdata: {
            type: Object,
            default: null
        },
        options: {
            type: Object,
        }
    },
    mounted() {
        this.renderChart(this.chartdata, this.options)
    },
    watch: {
        // eslint-disable-next-line no-unused-vars
        "chartdata"(to, from) {
            this.renderChart(this.chartdata, this.options)
        }
    }
}