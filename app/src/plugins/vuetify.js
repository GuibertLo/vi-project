import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify)

const opts = {
    theme: {
        themes: {
            light: {
                primary: '#C9082A',
                secondary: '#17408B',
                accent: '#C9082A',
                error: '#b71c1c',
            },
        },
    },
}

export default new Vuetify(opts)