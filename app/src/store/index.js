import Vue from 'vue'
import Vuex from 'vuex'

import system from './modules/system'
import remote from './modules/remote'

Vue.use(Vuex)

// Mode of vuex
const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    modules: {
        system,
        remote,
    },
    plugins: [
    ],
    strict: debug,
})
