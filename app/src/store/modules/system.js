const state = {
    isDrawerActive: false,
    remoteDataStatus: {
        areAllSeasonsLoaded: true,
        value: 0
    },
    isComparisonActivated: false,
    firstSelection: {
        object: null,
        mode: 1,
    },
    secondSelection: {
        object: null,
        mode: 1,
    },
    activeSeason: '2017-18',
    binSize: 10,
    activePoints: 'total'
}

// getters
const getters = {

    isDrawerActive() {
        return state.isDrawerActive
    },

    isRemoteDataLoaded() {
        let allDownloaded = null
        if (state.remoteDataStatus.areAllSeasonsLoaded) {
            allDownloaded = 511
        }
        else {
            allDownloaded = 7
        }
        return state.remoteDataStatus.value === allDownloaded
    },

    isComparisonActivated() {
        return state.isComparisonActivated
    },

    getFirstSelection() {
        return state.firstSelection
    },

    getSecondSelection() {
        return state.secondSelection
    },

    getActiveSeason() {
        return state.activeSeason
    },

    getBinSize() {
        return state.binSize
    },

    getActivePoints() {
        return state.activePoints
    }

}

const actions = {

    changeActiveSeason({ commit }, newSeason) {
        commit('changeActiveSeason', newSeason)
    },

    switchDrawer({ commit }) {
        commit('switchDrawer')
    },

    setRemoteDataLoaded({ commit }, boolean) {
        commit('setRemoteDataLoaded', boolean)
    },

    setComparison({ commit }, boolean) {
        commit('setComparison', boolean)
    },

    setFirstSelection({ commit }, payload) {
        commit('setFirstSelection', payload)
    },

    setSecondSelection({ commit }, payload) {
        commit('setSecondSelection', payload)
    },

    setSeason({ commit }, payload) {
        commit('setSeason', payload)
    },

    setLoadedObject({ commit }, { season, object }) {
        commit('setLoadedObject', { season, object })
    },

    setBinSize({ commit }, payload) {
        commit('setBinSize', payload)
    },

    setActivePoints({ commit }, payload) {
        commit('setActivePoints', payload)
    }

}

const mutations = {

    changeActiveSeason(state, newSeason) {
        state.activeSeason = newSeason
    },

    switchDrawer(state) {
        state.isDrawerActive = !state.isDrawerActive
    },

    setRemoteDataLoaded(state) {
        state.isRemoteDataLoaded = state
    },

    setComparison(state, payload) {
        state.isComparisonActivated = payload
    },

    setFirstSelection(state, payload) {
        if (payload.object) {
            state.firstSelection.object = payload.object
        }
        if (payload.mode) {
            state.firstSelection.mode = payload.mode
            state.firstSelection.object = null
        }
    },

    setSecondSelection(state, payload) {
        if (payload.object) {
            state.secondSelection.object = payload.object
        }
        if (payload.mode) {
            state.secondSelection.mode = payload.mode
            state.secondSelection.object = null
        }
    },

    setSeason(state, payload) {
        state.activeSeason = payload
    },


    setLoadedObject(state, payload) {
        let season = null
        let object = null
        switch (payload.season) {
            case "2017-18":
                season = 0
                break
            case "2018-19":
                season = 1
                break
            case "2019-20":
                season = 2
                break
            default:
                return
        }

        switch (payload.object) {
            case "players":
                object = 0
                break
            case "teams":
                object = 1
                break
            case "leagues":
                object = 2
                break
            default:
                return
        }
        state.remoteDataStatus.value |= (0b1 << season * 3 + object)
    },


    setBinSize(state, payload) {
        state.binSize = payload
    },


    setActivePoints(state, payload) {
        if (payload === 'total')
            state.activePoints = 'total'
        if (payload === 'success')
            state.activePoints = 'success'
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
