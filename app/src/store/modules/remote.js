import * as HTTPService from '@/data/rest'


const state = {
    leagues: {},
    shoots: null,
    players: {},
    passes: null,
    teams: {},
    seasons: ['2017-18', '2018-19', '2019-20']
}

// getters
const getters = {

    getPlayers() {
        return state.players
    },

    getTeams() {
        return state.teams
    },

    getLeagues() {
        return state.leagues
    },

    getSeasons() {
        return state.seasons
    }

}

const actions = {

    initObjects({ commit }) {
        commit('initObjects')
    },

    savePlayers({ commit }, { payload, season }) {
        commit('savePlayers', payload, season)
    },

    saveLeagues({ commit }, { payload, season }) {
        commit('saveLeagues', { payload, season })
    },

    saveTeams({ commit }, { payload, season }) {
        commit('saveTeams', payload, season)
    },

    /**
     * Use the server API to get all data and to store it
     * Use the HTTPService.processError method if failure
     * @param   commit, given by default (not passed on call)
     * @return  None, but launch the store's addResident commit
     */
    fetchAllData({ commit }, season) {
        HTTPService.HTTPGetPlayers(season)
            .then(item => {
                commit('savePlayers', { item, season })
            })
            .then( () => {
                commit('setLoadedObject', { season: season, object: "players"})
            })
            .catch(function (error) {
                HTTPService.processError(error, "get")
            })
        HTTPService.HTTPGetTeams(season)
            .then(item => {
                commit('saveTeams', { item, season })
            })
            .then( () => {
                commit('setLoadedObject', { season: season, object: "teams"})
            })
            .catch(function (error) {
                HTTPService.processError(error, "get")
            })
        HTTPService.HTTPGetLeagues(season)
            .then(item => {
                commit('saveLeagues', { item, season })
            })
            .then( () => {
                commit('setLoadedObject', { season: season, object: "leagues"})
            })
            .catch(function (error) {
                HTTPService.processError(error, "get")
            })
    },

}

const mutations = {

    initObjects(state) {
        state.seasons.forEach(season => {
            state.players[season] = {}
            state.teams[season] = {}
            state.leagues[season] = {}
        })
    },

    savePlayers(state, payload) {
        if (payload.season) {
            payload.item.forEach(player => {
                if (player["rowSet"].length !== 0)
                    state.players[payload.season][player["rowSet"][0][0]] = player
            })
        }
    },

    saveTeams(state, payload) {
        if (payload.season) {
            payload.item.forEach(team => {
                if (team["rowSet"].length !== 0)
                    state.teams[payload.season][team["rowSet"][0][2]] = team
            })
        }
    },

    saveLeagues(state, payload) {
        if (payload.season) {
            state.leagues[payload.season] = payload.item
        }
    },

}

export default {
    state,
    getters,
    actions,
    mutations
}
