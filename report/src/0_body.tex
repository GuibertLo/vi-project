\section{Introduction}

This project aims to deepen the visualization skills of master students. The goal is to fill a specific need for a person or a group of person using visual representations brought by a technological tool.

In our group, we wanted to chose a subject according to our hobbies or passions. Being two current or former basketball players, we decided to focus on this domain. The NBA being the most famous basketball league in the world, we have chosen this topic to develop.

\subsection{Data selection}
All the information related to NBA games are available though NBA-provided endpoints. Depending on the language used to access the data, APIs may exist to make the fetching of data easier.

Now down to the data, the collection of data being really huge -- one can access to players shots/rebound/passes statistics, teams information, images, logos, schedules, teams standing, and more -- we selected the fields that seemed relevant for the message we wanted to convey. These are the selected fields:
\begin{itemize}
    \item Players made and missed shots positions
    \item Teams seasons statistics
    \item Overall league statistics
\end{itemize}

\subsection{Intention / message to convey}

The aim of the visualization is to provide a representation of the NBA shots statistics.
Anyone can compare the shot locations between players or teams, depending on the granularity of data selected by the user. The purpose of this comparison tool is to allow NBA teams, mainly the coaches, to analyze the way other teams/players generally play.

\newpage

\section{Used tools}

\subsection{Server application}

In first, we wanted to use unofficial \texttt{npm} packages in order to get the \texttt{NBA} shoot data, because the league does not provide any of them. But during our development phase, we noticed that the \texttt{NBA} \texttt{API} is very unstable: sometimes, we got timeouts on the client application, but not on our Python script.

In order to fix this issue, we decided to create a backend server that serves the data. We initially extracted all we need through the Python script to \texttt{json} files, that are then stored in a folder. Then, we created a \texttt{nodejs} server and created three endpoints, one for each data fields we need: the league, the teams and the players. Their data is reachable using \texttt{HTTP} \texttt{GET} requests. The season can be given by a parameter in the \texttt{URL}, after the indicator \texttt{?season=...}. We can give either give the values "2017-18", "2018-19" or "2019-20" and the system adapts itself.

\subsection{Client application}

Based on our previous projects and our experience, we decided to develop the client application using the \texttt{Vue}\footnote{source: \url{https://vuejs.org/}} framework. Based on the \texttt{Javascript} language, it allows to build complete web applications easily. 

This technology is also an adapted choice for us, because we wanted to provide an accessible tool to our users: indeed, a web application is easy to access through a browser and a domain name. Nothing must be downloaded and installed on the user machine.

Here are some additional modules we used:
\begin{itemize}
    \item \textbf{Vuetify}\footnote{source: \url{https://vuetifyjs.com/}}: a graphical framework that brings a lot of pre-made and reusable components, that follow the \texttt{Material Design} \footnote{source: \url{https://material.io/design}} guidelines.
    \item \textbf{Axios}\footnote{source: \url{https://axios-http.com/}}: a \texttt{HTTP(S)} client based on \texttt{premises}, with useful features and various parameters.
    \item \textbf{Chart.js}\footnote{source: \url{https://www.chartjs.org/}}: a complete library that brings all kind of graphs in a \texttt{Javascript} environment. It is highly customizable.
    \item \textbf{vue-chartjs}\footnote{source: \url{https://vue-chartjs.org/}}: integrates the \texttt{Chart.js} library into a \texttt{Vue} environment, that allows deeper integration in the system.
    \item \textbf{Vue Router}\footnote{source: \url{https://router.vuejs.org/}}: an official module that allows to navigate through views in the application, using custom rules.
    \item \textbf{Vuex}\footnote{source: \url{https://vuex.vuejs.org/}}: an official module that manages the state of the application. It can handle data synchronously or not, and brings listeners on its changes.
    \item And various useful modules that are not relevant to present here.
\end{itemize}

The choices of those modules were motivated by their popularity and features. For the most of them, they are largely used and fully integrated in \texttt{Vue} applications. We could have not use some of them, like the \texttt{Vue Router} or the \texttt{Vuex} ones, but their integration make the whole project more stable, more robust and scalable.

Regarding the choice of the \texttt{Chart.js} module, we selected it because it includes the type of plots we wanted to exploit. Moreover, it is very flexible and can adapt itself to various conditions. It is also largely adaopted in the \texttt{Javascript} online community.

\newpage

\section{Representation choices}

Based on the message to convey, we thought about various ways to represent our data in a readable, comprehensive and clean matter. We tried various shapes, dimensions, plots, colors and interactions, and we finally found the best choice for our goal.

\subsection{Granularity of the data}

As described before, our data is granular: we extracted various version of it, depending on the entity we consider:
\begin{itemize}
    \item \textbf{Overall league}: all the shots made in a entire season, by all the registered players of the \texttt{nba} league
    \item \textbf{Team-specific}: all the shots made in a entire season, by all the players in a team
    \item \textbf{Player-specific}: all the shots made in a entire season, by a single player
\end{itemize}

\subsection{Main visualization}

By the nature of the insights we want to convey, we had to find a way to represent a basketball half court plot. Indeed, the positions of the shots must be integrated in a virtual dimension that is friendly and correct for the user. Thus, the background is a basketball field in order to ease the understanding of points locations.

\begin{figure}[H]
    \centering
    \includegraphics[width=6cm]{path31.png}
    \caption{Basketball field used as chart's background.}
\end{figure}

Some shots are taken from the other side of the court, but we have chosen to ignore them. Indeed, this kind of shots are taken on the clock, at the end of the timer, and are meaningless in a scope of performance analysis.

Once the half court integrated, we had to chose the most appropriate plot, or chart type, in order to give indications on the shots. The most intuitive way to represent 2D information is on a 2D chart.

For each shot taken by a player, four information are available: x and y field position, the corresponding field-sector's name and if the shot has been successful or not. Nevertheless, the number of shots for a given player during a whole season being really huge, we had to aggregate shots locations since many of them are close to each other. This led us to set the importance of the points on the 2D charts proportional to the number of shots taken in the relevant area.

As data is not time related but contains independents events happening in the same space, we decided to show the shots locations as a scatter plot, as known as a bubble chart. But while using this plot type, we realized that the overall representation was not convincing. It does give visual insights on the position of the field where most shots happen, but the size of the points or bubbles where complex to manage and not really appropriate.

We then tried to draw a heat map, which indicates the intensity of an event as colored cases in two dimensions. By this approach, the field coverage is better distributed and the insights we want to communicate are clearer. Therefore, we kept this plot type and tuned it in order to be as adapted as possible to our case.

\begin{figure}[H]
    \centering
    \includegraphics[width=10cm]{plot_main.png}
    \caption{Main visualization example}
    \label{fig:plot_main}
\end{figure}

\subsection{Secondary perspective}
The first representation is considering a 2D space to show the shot locations. From a player point-of-view, one of the main aspects to consider while shooting is the distance.

Thus, we added a secondary perspective, showing either the number of shot taken, or rate of made shots according to the distance to the hoop. This allow the user to quickly see what are the overall shooting positions on the field.
Also, this simple visualization made possible to add the mean shot position, which is a great indicator of the way the entity (league, team or player) behaves on the field.

\begin{figure}[H]
    \centering
    \includegraphics[width=10cm]{plot_secondary.png}
    \caption{Secondary visualization example}
    \label{fig:plot_secondary}
\end{figure}

\subsection{Labeling}
As you can see on figures \ref{fig:plot_main} and \ref{fig:plot_secondary}, we included labels in order to explain the signification of the used colors and their meaning on the data. This brings useful and important context to the plots.

\subsection{Comparison}
As we want to allow a complete experience on an entire sport league, we wanted to bring the possibility to display two entities in order to make comparison possible.

We provided a switch in order to activate, or not, this option. When active, the view splits itself in order to add another visualization of the half court, with independent data. In order to implement it, we added a simple switcher, that can be either active or inactive.

We intentionally limited the comparison to two courts, otherwise the view becomes very loaded and the plots become too small.

\subsection{Interactions}

By the nature of the data we display, we do not have a lot of possible interactions. We nevertheless managed to integrate some in order to increase the insights on our information.

We do not have any scrolling on the data, because all insights are displayed in its granularity on our plots. However, depending on the monitor resolution of the user's browser, it is possible that he or she has to scroll down the page to access to all our visualizations.

\subsubsection{Plot-related}

On a larger scale, we provided transformations on the entire visualization. As described before, we have different granularity of our data: we start with the whole \texttt{nba} league, then all its teams and finally all its players. The initial view is set on the complete overview, on our case the whole league. Then, using controls, the user can chose to zoom in the data by choosing either a team or a player, using the corresponding controls in our application. This way, we provide several level of details for our representation and bring the possibility to fully explore the whole dataset.

We used a tabulation selector, because there is only three choices in this control form and it brings the complete scope of the possible choices to the user. Moreover, the change can be done in only one click. Each choice is only applied to the corresponding plot, in order to enable more possibilities of comparisons (whole league versus a team, a player team versus the player only, ...).

This interaction includes a search feature, that allows users to fill in the name of a team or a player in order to select it or him. Some browsing is also possible, the entire list of entities being displayed on the corresponding form.

\begin{figure}[H]
    \centering
    \includegraphics[width=12cm]{granularity.png}
    \caption{An example of the granularity for interactions}
    \label{fig:granularity}
\end{figure}

\subsubsection{System-wide}

First of all, we integrated some details on demand. By hovering a point in the chart, the user is able to get some information on shots' statistics at the given location and the corresponding point is shown. This is made possible by the integration of a tool-tip which brings those details. We kept it light and simple in order to limit the quantity of information on the screen.

\begin{figure}[H]
    \centering
    \includegraphics[width=4cm]{tooltip_main.png}
    \caption{An example of the details on the main plot}
    \label{fig:tooltip_main}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=4cm]{tooltip_secondary.png}
    \caption{An example of the details on the secondary plot}
    \label{fig:tooltip_secondary}
\end{figure}

Furthermore, we added a temporal dimension on our data by letting the user chose between the last three seasons. By this interaction, we allow to bring a notion of evolution through time of our insights. We implemented it in the form of a tabulation selector, for the same reasons that for the granularity of the data. There is also only three choices, that are applied to all the plots.

The comparison mode can be activated or not, like presented before in this document.

We also added an option to change the heat map values: by default, the colors are set depending on the relative value of successful shots on total shots made on a position. We added the possibility to change this scale by taking only the successful shots, which brings an absolute point of view on the same information. This mode can be changed by using a tabulation selector, for the same reasons that for the granularity of the data.

Finally, we added an option that lets users change the size of the shooting position bins. This simple parameter can help to identify larger or smaller deciding zones of successful or numerous shots. We implemented this control form using a slider, that indicates the current selected value.

\begin{figure}[H]
    \centering
    \includegraphics[width=16cm]{filters.png}
    \caption{An example of the filters for interactions}
    \label{fig:filters}
\end{figure}

\subsection{Navigation}

Our client application only contains two pages, because we managed to obtain a complete thus light single view to provide our insights. A second page exists and gives information about the project, but it does not contain any representation.

In a way, our chart was made navigable by including various filters, by using sliders, tabs or text fields.

\subsection{GUI}

The Graphical User Interface has been conceived in a simple way, without loading the whole display. We grouped the similar elements of the application, such as the controls of the filter or the graphs.

We defined a grid of twelve columns in order to organize the content in a coherent manner. 

\subsection{Gestalt principles applied}
Even if the visualization seams quite simple, using basic elements, such as heat-map or scatter plots, it makes a good use of various \textit{gestalt} principles:
\begin{itemize}
    \item Overview and detail: as the user is able to choose to visualize date from a league-perspective to a single-player one, it changes the quantity of data by zooming in and out on it. Also, when changing the grid size with the slider, the user can either have a summarized or a detailed view of the shots locations.
    \item Similarity: on the heat-map, locations that have the same color represents the same quantity. Also, on the scatter plot, the mean position is represented as a dot in a different color and size compared to the other points. The brain than creates two set of data: the blue dots, and the red one, giving a different sense to each set.
    \item Symmetry: when using the comparison mode, the screen splits in two in a very symmetrical way. The human eye tend to prefer symmetrical designs to more chaotic, or random, ones.
    \item Continuity: on the scatter plot, the points represent either number of shots taken or made a given distance from the hoop. The points are not linked with a line, but the human brain instinctively connect them to form a continuous set of data.
\end{itemize}

\subsection{Colors considerations}
Not all people see the color the same way. When around 92\% of the population interpret colors the same way, there is around 7 to 8\% of the population that is "color blind".

It is important to take these people in account when deciding which colors will be used in a visualization. If it is not done, colors blinds may experiment difficulties to have a quick insight on the visualization, or even may not understand what is represented.

Here is what different kinds of colorblindness would see compared to the visual color spectrum \footnote{source: \url{https://iristech.co/why-red-green-colorblind-individuals-are-more-common/}}:

\begin{figure}[H]
    \centering
    \includegraphics[width=8cm]{color-blindness-types.png}
    \caption{Normal vision compared to 6 different colorblindness types.}
    \label{fig:color-blindess-types}
\end{figure}

The color palette that has been chosen for the representation is the following one: 

\begin{figure}[H]
    \centering
    \includegraphics[width=10cm]{palette.png}
    \caption{Selected color palette}
\end{figure}

The selected colors should be contrasted enough for everyone to see the differences \footnote{To visualize colorblindness types: \url{https://davidmathlogic.com/colorblind/}}. The colors used in the heat map goes from the first color (blue-ish) to the third one (pink-ish). As the color of a given point is scaled between these two colors, it may contain subtle tints that a colorblind would not distinguish well from other color used.

\begin{figure}[H]
    \centering
    \includegraphics[width=12cm]{gradient_band.png}
    \caption{Color scale for the heat map. It goes through the first three colors of the palette.}
    \label{fig:gradient_band}
\end{figure}

\subsection{Data source mention}
We mentioned the data source in our client application, but not in the main view: indeed, we thought that keeping the main page light was a good idea, and we already had an "About" page. Therefore, we indicated the data source by a link in this page.

\newpage
\section{Conclusion}

\subsection{Encountered problems}
During the project, we encountered many obstacles:
\begin{itemize}
    \item We first tested the access to the data through a Python script to get a hand on the data. Although we knew the data was accessible, when trying to access it either via npm packages or via web requests in JavaScript, the nba api had and inconsistent behavior, returning results from time to time. After many tries we decided to collect all the data needed via the previous Python script and to load the data using a backend server.
    \item Before displaying the shots locations as a heatmap, we first showed as a bubble chart. When switching to heatmap, we discovered that it was very difficult to switch due to version conflicts between the package and Vue. We had to create the heatmap our own way.
    
\end{itemize}

\subsection{Future work}
Even if a lot of our objectives for the project have been accomplished, there is always work to be done.

Firstly, the comparison tool shows two separate views that are completely independent. For a better user experience, they should be linked. For example, when hovering a data on the either heat-map, it should show a tool-tip for both of them at the same position.

Secondly, the data is currently stored locally, and a backend-server has to run to provide information to the frontend. This is quite constraining: we have only access to the data we have downloaded. A better solution would be to make GET requests directly to the nba API, in order to access data for every year, team, and player in the nba history. 

We could add more statistics to our tool by collecting data on rebounds or passes in order to bring new views and insights to the users. This kind of data does already exist, we would just have to process them and to chose the best representation for them.

\subsection{Project feedback}

This project was very fun to develop. We enjoyed its scope, its technological part and its purpose. The fact that the subject of the \textit{Information Visualization} project was free for the group, as long as it is in the course topic, is a really good point and it motivates the students. We had enough time to work on the project, and the professor was always available if we had question. With those good conditions, we managed to produce a high-quality output.

The various choices we made did not block us in our work nor processes: if we would have to redo this project, we would have done the same choices. In a technical point of view, we did not had any major blocking problem. Sometimes, we had to use some tricks to resolve bugs or to implement a feature, but those tricks are light and do not impact the stability of our project.

As a team, we had no difficulties to work together. The group dynamic was really good, the workload was distributed equitably and the communication was perfect. Our profiles were complementary, which allowed us to operate efficiently and to help each other.